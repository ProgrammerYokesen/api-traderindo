<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Lab
Route::get('lab/follower', function () {
    $user = App\User::find(6);
    $follower = $user->followers->pluck('follower_id')->toArray();
    $receiver = App\fcmKey::with('user')->whereIn('user_id', $follower)->get();
    $receiver = $receiver->map(function ($receiver) {
        if ($receiver->user->notif == 1) {
            return $receiver->token;
        }
    })->toArray();
    return gettype($receiver);
    return $receiver;
});
Route::get('lab/signal', function () {
    $username =App\Signal::find(11)->user->name;
    $recipients = [
        $username
    ];
    return $recipients;
});
// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('register', 'userController@register');
Route::post('google-sign-in', 'userController@googleSignIn');
Route::post('login', 'userController@login');
// Route::post('validation', 'userController@registerValidation');
Route::get('category', 'signalController@cat')->middleware('jwt.verify');
Route::post('notif', 'userController@notif')->middleware('jwt.verify');
Route::get('profile/{id}', 'userController@profile')->middleware('jwt.optional');
Route::post('edit-profile', 'userController@editProfile')->middleware('jwt.verify');
Route::get('user', 'userController@allUser')->middleware('jwt.optional');
Route::get('signals', 'signalController@index')->middleware('jwt.optional');
Route::post('signals', 'signalController@store')->middleware('jwt.verify');
Route::get('signal/{id}', 'signalController@signalDetail')->middleware('jwt.optional');
Route::delete('signal/{id}', 'signalController@delete')->middleware('jwt.verify');
Route::put('signal/{id}', 'signalController@edit')->middleware('jwt.verify');
Route::post('comment', 'commentController@store')->middleware('jwt.verify');
Route::post('comment/like', 'commentController@storeLike')->middleware('jwt.verify');
Route::post('comment/child-comment', 'commentController@storeChildComment')->middleware('jwt.verify');
Route::get('comment/{id}', 'commentController@index')->middleware('jwt.optional');
Route::post('like', 'signalController@reaction')->middleware('jwt.verify');
Route::post('comment/child-comment/like', 'commentController@storeChildCommentLike')->middleware('jwt.verify');
Route::post('follow', 'followerController@store')->middleware('jwt.verify');
Route::post('bookmark', 'bookmarkSignalController@store')->middleware('jwt.verify');
Route::get('bookmark', 'bookmarkSignalController@index')->middleware('jwt.verify');

Route::get('notification','notificationController@index')->middleware('jwt.verify');

//Lab FCM
Route::get('fcm', 'userController@fcm');

//Forgot password Route
Route::post('send-token-forgot-password','userController@sendMailforgotPassword');
Route::post('forgot-password','userController@forgotPassword');