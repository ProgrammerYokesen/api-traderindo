<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SignalReactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signal_reactions', function (Blueprint $table) {
            $table->id();
            $table->integer('signal_id');
            $table->integer('cms_user_id');
            $table->enum('reaction', ['love', 'happy', 'sad', 'angry', 'confused']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signal_reactions');
    }
}
