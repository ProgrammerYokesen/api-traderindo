<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class signalReaction extends Model
{
    //
    protected $table = "signal_reactions";
    protected $guarded = [];

    public function signal(){
        return $this->belongsTo(\App\Signal::class);
    }
    public function user(){
        return $this->belongsTo(\App\User::class);
    }

}
