<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class childCommentLike extends Model
{
    //
    protected $table="childcomment_likes";
    protected $guarded = [];


    public function comment(){
        return $this->belongsTo(\App\comment_on_comment::class,'commentOnComment_id');
    }

}
