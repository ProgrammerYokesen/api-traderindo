<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class commentLike extends Model
{
    //
    protected $table="comment_likes";
    protected $guarded = [];

    public function comment(){
        return $this->belongsTo(\App\comment::class,'comment_id');
    }
    public function user(){
        return $this->belongsTo(\App\User::class,'user_id');
    }

}
