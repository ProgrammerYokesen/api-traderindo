<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Signal extends Model
{
    //
    use SoftDeletes;

    protected $table="signals";

    protected $appends = ['readTime', 'bobot', 'bookmarks', 'likes', 'totalComment', 'totalLike'];
    
    protected $guarded=[];
    
    protected $dates = ['deleted_at'];
    
    public function scopeBanner($query){
        return $query->where('banner', 1);
    }

    public function pair(){
        return $this->belongsTo(\App\pair::class,'item');
    }
    public function user(){
        return $this->belongsTo(\App\User::class,'user_id');
    }
    public function comment(){
        return $this->hasMany(\App\comment::class);
    }
    public function reaction(){
        return $this->hasMany(\App\signalReaction::class);
    }
    public function bookmark(){
        return $this->hasMany(\App\bookmarkSignal::class, 'signal_id');
    }

    public function getreadTimeAttribute(){
        $totalWord=str_word_count($this->artikel);
        $mostReadTime=$totalWord/255;
        $mostReadTime=ceil($mostReadTime);
        $readingTime=$mostReadTime;
        return $readingTime;
    }
    public function gettotalCommentAttribute(){
        return $this->comment->count();
    }
    public function gettotalLikeAttribute(){
        return $this->reaction->count();
    }
    // public function getbobotAttribute(){
    //     $bobot = ($this->totalLove * 2) + $this->totalHappy - $this->totalSad - ($this->totalAngry * 2);
    //     return $bobot;
    // }
    public function getbobotAttribute(){
        return 10;
    }
    public function getbookmarksAttribute(){
        if($this->bookmark->where('user_id', \Auth::id())->count()>0){
            return true;
        }
        return false;
            
    }
    public function getlikesAttribute(){
        if($this->reaction->where('cms_user_id', \Auth::id())->count()==1){
            return true;
        }
        return false;
    }
    public static function signalPosisition($id){
        $signal = signal::find($id);
        if($signal->user_id == \Auth::id()){
            return $signal->status;
        }
            return 'closed';
    }
}
