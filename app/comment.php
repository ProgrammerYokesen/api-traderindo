<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class comment extends Model
{
    //
    protected $appends = ['name', 'username', 'photo', 'totalLike', 'likes'];
    protected $guarded = [];
    
    protected $hidden = ['like'];
    public function signal(){
        return $this->belongsTo(\App\pair::class);
    }

    public function getnameAttribute(){
        return User::findOrFail($this->cms_user_id)->name;
    }
    public function getusernameAttribute(){
        return User::findOrFail($this->cms_user_id)->username;
    }
    public function getphotoAttribute(){
        return User::findOrFail($this->cms_user_id)->photo;
    }
    public function childComment(){
        return $this->hasMany(\App\comment_on_comment::class,'comment_id');
    }
    public function user(){
        return $this->belongsTo(\App\User::class,'cms_user_id');
    }
    public function like(){
        return $this->hasMany(\App\commentLike::class,'comment_id');
    }
    public function gettotalLikeAttribute(){
        return $this->like->count();
    }
    public static function format($data){
        // return $data->child_comment;
        return [
            'id_comment' => $data->id,
            'signal_id' => $data->signal_id,
            'comment' => $data->comment,
            'userName' => $data->userName,
            'totalLike' => $data->totalLike,
            'totalDislike' => $data->totalDislike,
            'child_comment' => $data['child_comment'],
            'createdAt' => $data->created_at,
            'lastUpdate' => $data->updated_at->diffForHumans()
        ];
    }
    public function getlikesAttribute(){
        if($this->like->where('user_id', \Auth::id())->count()==1){
            return true;
        }
        return false;
    }
}
