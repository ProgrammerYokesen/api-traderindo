<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Signal;

class bookmarkSignal extends Model
{
    //
    protected $table = 'bookmark_signals';
    // protected $appends = ['details'];
    protected $guarded = [];

    public function user(){
        return $this->belongsTo(\App\User::class, 'user_id');
    }
    public function signal(){
        return $this->belongsTo(\App\Signal::class, 'signal_id');
    }
    // public function getdetailsAttribute(){
    //     return Signal::find($this->signal_id);
    // }
}
