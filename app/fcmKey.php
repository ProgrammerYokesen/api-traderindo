<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class fcmKey extends Model
{
    use SoftDeletes;

    protected $table = 'fcm_key';
    
    protected $guarded = [];

    public function user(){
        return $this->belongsTo(\App\User::class,'user_id');
    }

}
