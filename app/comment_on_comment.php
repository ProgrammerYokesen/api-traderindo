<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class comment_on_comment extends Model
{
    
    protected $appends = ['name', 'username', 'photo', 'totalLike', 'likes'];
    protected $guarded = [];
    protected $hidden = ['like'];
    public function parentComment(){
        return $this->belongsTo(\App\comment::class,'comment_id');
    }

    public function getnameAttribute(){
        return User::findOrFail($this->cms_user_id)->name;
    }
    public function getusernameAttribute(){
        return User::findOrFail($this->cms_user_id)->username;
    }
    public function getphotoAttribute(){
        return User::findOrFail($this->cms_user_id)->photo;
    }
    public function like(){
        return $this->hasMany(\App\childCommentLike::class,'commentOnComment_id');
    }
    public function gettotalLikeAttribute(){
        return $this->like->count();
    }
    public function getlikesAttribute(){
        if($this->like->where('user_id', \Auth::id())->count()==1){
            return true;
        }
        return false;
    }

}
