<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Signal;
use App\pair;
use App\signalReaction;
use App\fcmKey;
use App\followers;
use App\bookmarkSignal;
use Illuminate\Support\Facades\Auth;

class bookmarkSignalController extends Controller
{
    //
    public function store(Request $request){
        $validator = \Validator::make($request->all(), [
            'signalId' => 'required|integer',
        ]);
        
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        
        if(\Auth::user()->bookmark->where('signal_id', $request->signalId)->count() > 0){
            //Soft delete here
            $bookmark = bookmarkSignal::where('user_id', \Auth::id())->where('signal_id', $request->signalId)->delete();
                return response()->json([
                    'status' => 'Success',
                    'message' => 'Un-bookmark success'
                ],200);
        }
        bookmarkSignal::create([
            'user_id' => \Auth::id(),
            'signal_id' => $request->signalId
        ]);
        return response()->json([
            'status' => 'Success',
            'message' => 'Bookmark success'
        ],200);
    }
    
    public function index(){
        $data = \Auth::user()->bookmark()->pluck('signal_id');
        $signal = Signal::with('user')->with('pair')->orderBy('signals.created_at', 'asc')->whereIn('id', $data)->latest()->get();
        $signal = $this->mapping($signal);
        return response()->json(compact('signal'), 200);
    }
    
    protected function mapping($signal)
    {
        return $signal->map(function ($signal) {
            return [
                'id' => $signal->id,
                'user_id' => $signal->user_id,
                'title' => $signal->judul,
                'type' => $signal->type,
                'price' => $signal->price,
                'pair' => $signal->pair->pair,
                'createdBy' => $signal->user->name,
                'readTime' => $signal->readTime,
                'bobot' => $signal->bobot,
                'show' => $signal->showResult,
                'bookmarks' => $signal->bookmarks,
                'createdAt' => $signal->created_at,
                'lastUpdate' => $signal->updated_at->diffForHumans()
            ];
        });
    }
}
