<?php

namespace App\Http\Controllers;

use App\User;
use App\attendance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\fcmKey;
use App\tokenForgotPassword;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        $credentials = $request->only('email', 'password');
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        $user = Auth::user();
        if($request->fcmToken){
            if(fcmKey::where('user_id', $user->id)->where('token', $request->fcmToken)->count()==0){
                fcmKey::create([
                    'user_id' => $user->id,
                    'token' => $request->fcmToken
                ]);
            }
        }
        return response()->json(compact('token', 'user'));
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:user_data_bank',
            'password' => 'required|string|min:6|confirmed'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //Save profile image
        $path = NULL;
        if ($request->file('image')) {
            //  return 'gambar';
            // $request->file('file');
            date_default_timezone_set("Asia/Jakarta");
            $file = $request->file('image');
            $uniqueFileName = str_replace(" ", "", time() . "-" . $file->getClientOriginalName());
            $path = $file->storeAs('public/profile-pict', time() . '-' . $uniqueFileName);
            $path = "storage/profile-pict/".$uniqueFileName;
            $path = 'https://api.traderindo.com/' . $path;
        } else if ($request->image) {

            $base64_image = $request->image;
            $base64_image = "data:image/png;base64," . $base64_image;

            if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                $data = substr($base64_image, strpos($base64_image, ',') + 1);

                $file = base64_decode($data);
                $uniqueFileName = "profile-pict" . time();

                \Storage::disk('local')->put("public/profile-pict/" . $uniqueFileName . ".png", $file);
                $path = "storage/profile-pict/" . $uniqueFileName . ".png";
                $path = 'https://api.traderindo.com/' . $path;
            }
        }
        // End save profile image
         $username = 'user' . random_int(1, 999999);
         
        if (User::uniqueUsername($request->username) == 'true') {
            if($request->username != NULL){
                $username = $request->username;
            }
            
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'username' => $username,
                'id_cms_privileges' => 2,
                'photo' => $path,
                'utm_campaign' => $request->utm_campaign,
                'utm_source' => $request->utm_source,
                'utm_medium' => $request->utm_medium,
                'utm_content' => $request->utm_content,
                'providerOrigin' => $request->provider_origin,
                'parent' => $request->ref_id
            ]);

            $token = \JWTAuth::fromUser($user);
            fcmKey::create([
                'user_id' => $user->id,
                'token' => $request->fcmToken
            ]);
            return response()->json(compact('user', 'token'), 200);
        } else {
            return response()->json("Username already taken", 422);
        }
    }

    public function getAuthenticatedUser()
    {
        try {

            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());
        }

        return response()->json(compact('user'));
    }

    public function profile($id)
    {
        $user = User::findOrFail($id);
        return response()->json(compact('user'));
    }

    public function allUser()
    {
        $user = User::all()->except(\Auth::id());
        return response()->json(compact('user'));
    }

    public function editProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'username' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        $user = \Auth::user();

        //Save profile image
        $path = $user->photo;
        if ($request->file('image')) {
            //  return 'gambar';
            // $request->file('file');
            date_default_timezone_set("Asia/Jakarta");
            $file = $request->file('image');
            $uniqueFileName = str_replace(" ", "", time() . "-" . $file->getClientOriginalName());
            $path = $file->storeAs('public/profile-pict', time() . '-' . $uniqueFileName);
            $path = "storage/profile-pict/".$uniqueFileName;
            $path = 'https://api.traderindo.com/' . $path;
        } else if ($request->image) {

            $base64_image = $request->image;
            $base64_image = "data:image/png;base64," . $base64_image;

            if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                $data = substr($base64_image, strpos($base64_image, ',') + 1);

                $file = base64_decode($data);
                $uniqueFileName = "profile-pict" . time();

                \Storage::disk('local')->put("public/profile-pict/" . $uniqueFileName . ".png", $file);
                $path = "storage/profile-pict/" . $uniqueFileName . ".png";
                $path = 'https://api.traderindo.com/' . $path;
            }
        }
        // End save profile image
        if (User::uniqueUsername($request->username) == 'true') {
            $update = $user->update([
                'name' => $request->name,
                'username' => $request->username,
                'deskripsi' => $request->desc,
                'photo' => $path
            ]);
            return response()->json([
                'message' => 'edit success',
                'user' => $update
            ])->setStatusCode(200);
        } else {
            return response()->json("Username already taken", 422);
        }
    }

    public function googleSignIn(Request $request)
    {
        // if ($request->tokenId == ENV('GOOGLE_KEY')) {
            if (User::where('email', $request->email)->count() == 1) {
                        $validator = Validator::make($request->all(), [
                        'email' => 'required|string|email',
                        'fcmToken' => 'required'
                    ]);
            
                    if ($validator->fails()) {
                        return response()->json($validator->errors(), 422);
                    }
                    //Login
                    $user = User::where('email', $request->email)->first();
                    if($user->username == NULL){
                        $username = 'user' . random_int(1, 999999);
                        User::where('email', $request->email)->update([
                                'username' => $username,
                            ]);
                    }
                    $token = \JWTAuth::fromUser($user);
                    if(fcmKey::where('user_id', $user->id)->where('token', $request->fcmToken)->count()==0){
                        fcmKey::create([
                            'user_id' => $user->id,
                            'token' => $request->fcmToken
                        ]);
                    }
                    return response()->json(compact('user', 'token'), 200);
            }
            //Register
                    $validator = Validator::make($request->all(), [
                        'name' => 'required|string|max:255',
                        'email' => 'required|string|email|max:255|unique:user_data_bank',
                        'photo' => 'required',
                    ]);
            
                    if ($validator->fails()) {
                        return response()->json($validator->errors(), 422);
                    }
            $username = 'user' . random_int(1, 999999);

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'photo' => $request->photo,
                'providerOrigin' => 'google',
                'username' => $username,
                'id_cms_privileges' => 2
            ]);
            if(fcmKey::where('user_id', $user->id)->where('token', $request->fcmToken)->count()==0){
                fcmKey::create([
                    'user_id' => $user->id,
                    'token' => $request->fcmToken
                ]);
            }
            $token = \JWTAuth::fromUser($user);

            return response()->json(compact('user', 'token'), 200);
        // } else {
        //     return response()->json([
        //         'Status' => 'Failed',
        //         'Message' => 'Cannot create account'
        //     ], 400);
        // }
    }

    public function notif(){
        $user = \Auth::user();
        if($user->notif == 1){
            $user->notif = 0;
            $user->save();
            return response()->json([
                'Status' => 'Success',
                'Message' => 'Sukses mematikan notifikasi aplikasi'
            ], 200);
        }
        else{
            $user->notif = 1;
            $user->save();
            return response()->json([
                'Status' => 'Success',
                'Message' => 'Sukses menyalakan notifikasi aplikasi'
            ], 200);
        }
    }

    public function fcm(){
        $recipients = [
            'dfSrjBvFSjeDlPiMrEv7rr:APA91bGAXmW-xJ2UxWVBqBCfWwF7J8Y1yjHcjbqfITA7qdeEuT8N2aFfTYFc8Q2TWIoIEigpfFaUxe1jfAHcB1y9ZsxCxnzN8XWLXThZM7J66Zj16jmMGdfoezBfBoStPqh8GFNb2Wa-',
        ];
        
        $data = fcm()
            ->to($recipients)
            ->priority('high')
            ->timeToLive(0)
            ->data([
                'signalId' => '1',
                'userId' => '21',
            ])
            ->notification([
                'title' => 'Test Lagi',
                'body' => 'This is a test of FCM',
            ])
            ->send();
        dd($data);
        return 'success';
    }

    public function sendMailForgotPassword(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $user = User::where('email', $request->email)->first();
        if($user){
            $token = md5(date('Y-m-d H:i:s').$user->name.$user->email).time();
            $user->forgotPasswordToken()->create([
                'token' => $token
            ]);
            return response()->json([
                'Status' => 'Success',
                'Message' => 'Token created'
            ]);
        }
        return response()->json([
            'Status' => 'Failed',
            'Message' => 'User not found'
        ]);
    }

    public function forgotPassword(Request $request){
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'password' => 'required|string|min:6|confirmed'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $user = tokenForgotPassword::where('token', $request->token)->first();
        if($user){
            User::where('id', $user->user_id)->update([
                'password' => bcrypt($request->password)
            ]);
    
            return response()->json([
                'Status' => 'Success',
                'Message' => 'Password has been changed'
            ]);
        }
        return response()->json([
            'Status' => 'Failed',
            'Message' => 'Token Not Found'
        ]);
        
    }
}
