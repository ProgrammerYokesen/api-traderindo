<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\followers;
use App\User;
use App\fcmKey;

class followerController extends Controller
{
    //
    public function store(Request $request){
        $validator = \Validator::make($request->all(), [
            'user_id' => 'required',
        ]);
        
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $user = \Auth::user();
        $userId = $user->id;
        $data = followers::where('user_id', $request->user_id)->where('follower_id', $userId)->get();
        if($data->isEmpty()){
            $data = followers::create([
                'user_id' => $request->user_id,
                'follower_id' => $userId
            ]);
            
            //Notif follow here
            $receiver = User::find($request->user_id);
            if($receiver->notif){
                $recipients = fcmKey::where('user_id', $receiver->id)->pluck('token')->toArray();
                fcm()->to($recipients)->priority('high')->timeToLive(0)
                    ->data([
                        'userId' => $userId,
                        'username' => $user->username,
                    ])
                    ->notification([
                        'title' => 'You have new follower',
                        'body' => $user->username.' started following you',
                    ])->send();
            }
            //End notif

            return response()->json([
                'message' => 'follow success',
                'data' => $data
            ],200);
        }
        else{
            $data = followers::where('user_id', $request->user_id)->where('follower_id', $userId)->delete();
            return response()->json([
                'message' => 'unfollow success'
            ],200);
        }
        

    }

}
