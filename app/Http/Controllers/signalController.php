<?php

namespace App\Http\Controllers;

use App\User;
use App\Signal;
use App\pair;
use App\signalReaction;
use App\fcmKey;
use App\followers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class signalController extends Controller
{
    //
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pairId' => 'required|integer',
            'action' => 'required|string|max:255',
            'tp' => 'required|string|max:255',
            'sl' => 'required|string|max:255',
            'title' => 'required|string|max:255',
            'article' => 'required|string',
            'image' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        //Save profile image
        $path = NULL;
        if ($request->file('image')) {
            //  return 'gambar';
            // $request->file('file');
            date_default_timezone_set("Asia/Jakarta");
            $file = $request->file('image');
            $uniqueFileName = str_replace(" ", "", time() . "-" . $file->getClientOriginalName());
            $file->storeAs('public/photos', time() . '-' . $uniqueFileName);
            $path = "storage/photos/".$uniqueFileName;
            $path = 'https://api.traderindo.com/' . $path;
        } else if ($request->image) {

            $base64_image = $request->image;
            $base64_image = "data:image/png;base64," . $base64_image;

            if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                $data = substr($base64_image, strpos($base64_image, ',') + 1);

                $file = base64_decode($data);
                $uniqueFileName = "signal-pict" . time();

                \Storage::disk('local')->put("public/photos/" . $uniqueFileName . ".png", $file);
                $path = "storage/photos/" . $uniqueFileName . ".png";
                $path = 'https://api.traderindo.com/' . $path;
            }
        }
        // End save profile image
        $userId = \Auth::id();
        $slug = \str_slug($request->title);
        $signal = Signal::create([
            'judul' => $request->title,
            'artikel' => $request->article,
            'item' => $request->pairId,
            'type' => $request->action,
            'takeprofit' => $request->tp,
            'price' => $request->price,
            'stoploss' => $request->sl,
            'image' => $path,
            'user_id' => $userId,
            'slug' => $slug,
            'status' => 'new',
            'showResult' => $request->show
        ]);
        /*Notif here*/
        $user = \Auth::user();
        $follower = $user->followers->pluck('follower_id')->toArray();
        $recipients = fcmKey::with('user')->whereIn('user_id', $follower)->get();
        $recipients = $recipients->map(function ($recipients) {
            if ($recipients->user->notif == 1) {
                return $recipients->token;
            }
        })->toArray();
        fcm()->to($recipients)->priority('high')->timeToLive(0)
            ->data([
                'signalId' => $signal->id,
            ])
            ->notification([
                'title' => $user->username . ' post new signal',
                'body' => $signal->title,
            ])->send();
        /*End notif*/
        return response()->json([
            'status' => 'success',
            'message' => 'add success',
            'signal' => $signal
        ], 200);
    }
    public function index(Request $request)
    {
        $signal = Signal::query();
        $signal = Signal::with('user')->with('pair')->orderBy('signals.created_at', 'asc');

        if ($request->get('following') == 'true') {
            $user = \Auth::user();
            $userIds = $user->following()->pluck('user_id');
            $signal = $signal->whereIn('user_id', $userIds)->latest()->get();
            $signal = $this->mapping($signal);
            return response()->json(compact('signal'), 200);
        }
        if($request->get('banner') == 'true'){
             $signal = $signal->banner()->get();
             $signal = $this->mapping($signal);
             return response()->json(compact('signal'), 200);
        }
        if ($request->get('user_id')) {
            $signal = $signal->where('user_id', $request->user_id)->latest()->get();
            $signal = $this->mapping($signal);
            return response()->json(compact('signal'), 200);
        }

        if ($request->get('action')) {
            $signal = $signal->where('type', $request->action)->latest()->get();
            $signal = $this->mapping($signal);
            return response()->json(compact('signal'), 200);
        }

        if ($request->get('pair')) {
            $pair = pair::where('pair', $request->pair)->first();
            if ($pair == NULL) {
                return response()->json([
                    'status' => 'failed',
                    'message' => 'pair does not exist'
                ], 200);
            }
            $signal = $signal->where('item', $pair->id)->get();
            $signal = $this->mapping($signal);
            return response()->json(compact('signal'), 200);
        }
        $signal = $signal->get();
        $signal = $this->mapping($signal);
        return response()->json(compact('signal'), 200);
    }

    public function signalDetail($id)
    {
        $signalDetail = Signal::with('bookmark')->find($id);

        if ($signalDetail == NULL) {
            return response()->json(compact('signalDetail'), 200);
        }
        $signalDetail->views = $signalDetail->views + 1;
        $signalDetail->save();
        $signalDetail["user"] = $signalDetail->user;
        $signalDetail->pair;
        return response()->json(compact('signalDetail'), 200);
    }

    public function reaction(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'signalId' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
         if(\Auth::user()->reaction->where('signal_id', $request->signalId)->count() == 1){
             signalReaction::where('cms_user_id', \Auth::id())
                            ->where('signal_id', $request->signalId)
                            ->delete();
            return response()->json([
                'Status' => 'Success',
                'Message' => "Unlike success"
                ]);
         }
        $userId = \Auth::user()->id;
        $signalReaction = signalReaction::create([
                'signal_id' => $request->signalId,
                'cms_user_id' => $userId
            ]);
                /* Notif */
                $userId =Signal::find($request->signalId)->user->id;
                if($userId != \Auth::id()){
                    $recipients = fcmKey::where('user_id', $userId)->pluck('token')->toArray();
                    fcm()->to($recipients)->priority('high')->timeToLive(0)
                    ->data([
                        'signalId' => $request->signalId,
                    ])
                    ->notification([
                        'title' => \Auth::user()->username.' has reacted on your signal',
                        'body' => Signal::find($request->signalId)->judul,
                    ])->send();
                }
                /* End notif */
                    return response()->json([
                'Status' => 'Success',
                'Message' => "Like success"
                ]);
    }

    public function cat(Request $request)
    {
        if ($request->get('cat') == 'pairs') {
            return pair::where('status', 'active')->get();
        } elseif ($request->get('cat') == 'action') {
            $action = [];
            $action[0] = 'OP_SELL';
            $action[1] = 'OP_BUY';
            $action[2] = 'OP_SELLLIMIT';
            $action[3] = 'OP_BUYLIMIT';
            $action[4] = 'OP_SELLSTOP';
            $action[5] = 'OP_BUYSTOP';

            return response()->json(compact('action'), 200);
        }
    }

    protected function mapping($signal)
    {
        return $signal->map(function ($signal) {
            return [
                'id' => $signal->id,
                'user_id' => $signal->user_id,
                'userPhoto' => $signal->photo,
                'title' => $signal->judul,
                'article' => $signal->artikel,
                'image' => $signal->image,
                'type' => $signal->type,
                'price' => $signal->price,
                'pair' => $signal->pair->pair,
                'createdBy' => $signal->user->name,
                'readTime' => $signal->readTime,
                'bobot' => $signal->bobot,
                'show' => $signal->showResult,
                'totalComment' => $signal->totalComment,
                'totalLike' => $signal->totalLike,
                'bookmarks' => $signal->bookmarks,
                'likes' => $signal->likes,
                'createdAt' => $signal->created_at,
                'lastUpdate' => $signal->updated_at->diffForHumans()
            ];
        });
    }

    public function delete($id)
    {
        $signal = signal::find($id);
        if ($signal != NULL) {
            if ($signal->user_id == \Auth::id()) {
                $signal->delete();
                return response()->json([
                    'status' => 'Success',
                    'message' => 'Delete success'
                ])->setStatusCode(200);
            } else {
                return response()->json([
                    'status' => 'Failed',
                    'message' => 'Cannot delete this signal'
                ])->setStatusCode(200);
            }
        }
        return response()->json([
            'status' => 'Failed',
            'message' => 'Delete failed'
        ])->setStatusCode(200);
    }

    public function edit($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tp' => 'required|string|max:255',
            'sl' => 'required|string|max:255',
            'title' => 'required|string|max:255',
            'article' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        // Insert new image here
        $path = NULL;
        if ($request->file('image')) {
            date_default_timezone_set("Asia/Jakarta");
            $file = $request->file('image');
            $uniqueFileName = str_replace(" ", "", time() . "-" . $file->getClientOriginalName());
            $file->storeAs('public/photos', time() . '-' . $uniqueFileName);
            $path = "storage/photos/".$uniqueFileName;
            $path = 'https://api.traderindo.com/' . $path;
        } else if ($request->image) {
            if(strlen($request->image) == 0){
                $path = NULL;
            }
            else{
            $base64_image = $request->image;
            $base64_image = "data:image/png;base64," . $base64_image;

            if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                $data = substr($base64_image, strpos($base64_image, ',') + 1);

                $file = base64_decode($data);
                $uniqueFileName = "signal-pict" . time();

                \Storage::disk('local')->put("public/photos/" . $uniqueFileName . ".png", $file);
                $path = "storage/photos/" . $uniqueFileName . ".png";
                $path = 'https://api.traderindo.com/' . $path;
            }
            }
        }
        //End insert new image
        
        if (signal::signalPosisition($id) == NULL || signal::signalPosisition($id) == 'new') {
            $update = signal::where('id', $id)->update([
                'judul' => $request->title,
                'artikel' => $request->article,
                'takeprofit' => $request->tp,
                'price' => $request->price,
                'stoploss' => $request->sl,
                 'image' => $path,
                'showResult' => $request->show
            ]);

            /*Notif here*/
            $user = \Auth::user();
            $signal = signal::find($id);
            $follower = $user->followers->pluck('follower_id')->toArray();
            $recipients = fcmKey::with('user')->whereIn('user_id', $follower)->get();
            $recipients = $recipients->map(function ($recipients) {
                if ($recipients->user->notif == 1) {
                    return $recipients->token;
                }
            })->toArray();
            fcm()->to($recipients)->priority('high')->timeToLive(0)
                ->data([
                    'signalId' => $signal->id,
                ])
                ->notification([
                    'title' => $user->username . ' signal’s updated',
                    'body' => $signal->title,
                ])->send();
            /*End notif*/

            return response()->json([
                'status' => 'Success',
                'message' => 'Edit success'
            ])->setStatusCode(200);
        } elseif (signal::signalPosisition($id) == 'pending') {
            $update = signal::where('id', $id)->update([
                'judul' => $request->title,
                'artikel' => $request->article,
                'takeprofit' => $request->tp,
                'stoploss' => $request->sl,
                 'image' => $path,
                'showResult' => $request->show
            ]);
            /*Notif here*/
            $user = \Auth::user();
            $signal = signal::find($id);
            $follower = $user->followers->pluck('follower_id')->toArray();
            $recipients = fcmKey::with('user')->whereIn('user_id', $follower)->get();
            $recipients = $recipients->map(function ($recipients) {
                if ($recipients->user->notif == 1) {
                    return $recipients->token;
                }
            })->toArray();
            fcm()->to($recipients)->priority('high')->timeToLive(0)
                ->data([
                    'signalId' => $signal->id,
                ])
                ->notification([
                    'title' => $user->username . ' signal’s updated',
                    'body' => $signal->title,
                ])->send();
            /*End notif*/
            return response()->json([
                'status' => 'Success',
                'message' => 'Edit success'
            ])->setStatusCode(200);
        } else {
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'Cannot edit this signal'
            ])->setStatusCode(200);
        }
    }
}
