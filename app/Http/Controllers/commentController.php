<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\comment;
use App\comment_on_comment;
use App\commentLike;
use App\Signal;
use App\fcmKey;
use App\childCommentLike;
class commentController extends Controller
{
    public function index($id){
        $comment=comment::where('signal_id', $id)->with('childComment')->get();
  
        return response()->json(compact('comment'),200);
    }

    public function store(Request $request){
        $validator = \Validator::make($request->all(), [
            'signalId' => 'required',
            'comment' => 'required|string',
        ]);
        
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        

        /* Notif */
        $userId =Signal::find($request->signalId)->user->id;
        if($userId != \Auth::id()){
            $recipients = fcmKey::where('user_id', $userId)->pluck('token')->toArray();
            fcm()->to($recipients)->priority('high')->timeToLive(0)
            ->data([
                'signalId' => $request->signalId,
            ])
            ->notification([
                'title' => \Auth::user()->username.' has commented on your signal',
                'body' => $request->comment,
            ])->send();
        }
        /* End notif */
        $userId = \Auth::user()->id;
        $comment = Comment::create([
            'signal_id' => $request->signalId,
            'comment' => $request->comment,
            'cms_user_id' => $userId
        ]);

        return response()->json(compact('comment'),200);
    }

    public function storeChildComment(Request $request){
        $validator = \Validator::make($request->all(), [
            'commentId' => 'required',
            'comment' => 'required|string',
        ]);
        
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        $userId = \Auth::user()->id;
        $childComment = comment_on_comment::create([
            'comment_id' => $request->commentId,
            'comment' => $request->comment,
            'cms_user_id' => $userId
        ]);
                /* Notif */
                // $userId =Signal::find($request->signalId)->user->id;
                $userId = comment::find($request->commentId)->user->id;
                $signalId = comment::find($request->commentId)->signal_id;
                if($userId != \Auth::id()){
                    $recipients = fcmKey::where('user_id', $userId)->pluck('token')->toArray();
                    fcm()->to($recipients)->priority('high')->timeToLive(0)
                    ->data([
                        'signalId' => $signalId,
                    ])
                    ->notification([
                        'title' => \Auth::user()->username.' has commented on your comment',
                        'body' => $request->comment,
                    ])->send();
                }
                /* End notif */
        return response()->json(compact('childComment'),200);
    }

    public function storeLike(Request $request){
        $validator = \Validator::make($request->all(), [
            'commentId' => 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        if(\Auth::user()->commentLike->where('comment_id', $request->commentId)->count() == 1){
             commentLike::where('user_id', \Auth::id())
                            ->where('comment_id', $request->commentId)
                            ->delete();
            return response()->json([
                'Status' => 'Success',
                'Message' => "Unlike success"
                ]);
         }
        commentLike::create([
                'comment_id' => $request->commentId,
                'user_id' => \Auth::id()
            ]);
        return response()->json([
                'Status' => 'Success',
                'Message' => "Like success"
                ]);
    }

    public function storeChildCommentLike(Request $request){
        $validator = \Validator::make($request->all(), [
            'childCommentId' => 'required',
        ]);
        
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        if(\Auth::user()->childCommentLike->where('commentOnComment_id', $request->childCommentId)->count() == 1){
             childCommentLike::where('user_id', \Auth::id())
                            ->where('commentOnComment_id', $request->childCommentId)
                            ->delete();
            return response()->json([
                'Status' => 'Success',
                'Message' => "Unlike success"
                ]);
         }
        childCommentLike::create([
                'commentOnComment_id' => $request->childCommentId,
                'user_id' => \Auth::id()
            ]);
        return response()->json([
                'Status' => 'Success',
                'Message' => "Like success"
                ]);
    }
    

}
