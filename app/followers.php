<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class followers extends Model
{
    //
    protected $guarded=['created_at', 'updated_at'];
    public function userFollower(){
        return $this->belongsTo(\App\User::class, 'user_id');
    }
    public function userFollowing(){
        return $this->belongsTo(\App\User::class, 'follower_id');
    }
    public function token(){
        return $this->belongsTo(\App\fcmKey::class, 'follower_id');
    }
}
