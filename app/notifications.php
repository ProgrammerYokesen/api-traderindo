<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Signal;
use App\pair;

class notifications extends Model
{
    protected $table = 'notification';

    protected $guarded=['created_at', 'updated_at'];

    protected $appends = ['senderUsername', 'senderPhoto', 'signalName', 'signalAction', 'signalPair'];

    protected $hidden = ['sender', 'signal'];

    public function scopeunread($query)
    {
        return $query->where('read', 0)->where('receiverId', \Auth::id());
    }

    /**
     * Get the user that owns the notifications
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function receiver()
    {
        return $this->belongsTo(User::class, 'receiverId');
    }

    /**
     * Get the user that owns the notifications
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function signal()
    {
        return $this->belongsTo(Signal::class, 'signalId');
    }

    /**
     * Get the sender that owns the notifications
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sender()
    {
        return $this->belongsTo(User::class, 'senderId');
    }

    public function getsenderUsernameAttribute()
    {
        return $this->sender->name;
    }
    public function getsenderPhotoAttribute()
    {
        return $this->sender->photo;
    }
    public function getsignalNameAttribute()
    {
        return $this->signal->judul;
    }
    public function getsignalActionAttribute()
    {
        return $this->signal->type;
    }
    public function getsignalPairAttribute(){
        return pair::find($this->signal->item)->pair;
    }

}
