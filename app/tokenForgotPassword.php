<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tokenForgotPassword extends Model
{
    protected $table = "forgot_password";

    protected $fillable = ['token', 'user_id'];

    public function user(){
        return $this->belongsTo(\App\User::class, 'user_id');
    }

}
