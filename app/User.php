<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Support\Facades\DB;
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table="user_data_bank";
    protected $appends = ['totalSignal','totalFollowing','totalFollowers', 'follow'];

    protected $guarded = [
        'id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'id_cms_privileges',
        'status',
        'providerId',
        'providerOrigin',
        'photoId',
        'photoWithId',
        'whatsappVerifyStatus',
        'whatsappVerifyCode',
        'parent',
        'utm_source',
        'utm_medium',
        'utm_campaign',
        'utm_term',
        'utm_content',
        'uuid',
        'ipaddress',
        'screen',
        'platform',
        'platformVersion',
        'device',
        'browser',
        'browserVersion',
        'language',
        'robot',
        'signals',
        'followers',
        'following'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
    public function signals(){
        return $this->hasMany(\App\Signal::class,'user_id');
    }
    public function fcmKey(){
        return $this->hasMany(\App\fcmKey::class,'user_id');
    }
    public function comment(){
        return $this->hasMany(\App\comment::class,'cms_user_id');
    }
    public function reaction(){
        return $this->hasMany(\App\signalReaction::class,'cms_user_id');
    }
    public function commentLike(){
        return $this->hasMany(\App\commentLike::class,'user_id');
    }
    public function childCommentLike(){
        return $this->hasMany(\App\childCommentLike::class,'user_id');
    }
    public function followers(){
        return $this->hasMany(\App\followers::class,'user_id');
    }
    public function following(){
        return $this->hasMany(\App\followers::class,'follower_id');
    }
    public function bookmark(){
        return $this->hasMany(\App\bookmarkSignal::class,'user_id');
    }
    public function notification(){
        return $this->hasMany(\App\notifications::class, 'receiverId');
    }
    public function forgotPasswordToken(){
        return $this->hasMany(\App\tokenForgotPassword::class, 'user_id');
    }
    public static function uniqueUsername($username){
        if($username == NULL){
            return 'true';
        }
        $countUsername = User::whereNotIn('id', [\Auth::id()])->where('username',$username)->count();
        if($countUsername > 0 ){
            return 'false';
        }
        else{
            return 'true';
        }
    }
    public function gettotalSignalAttribute(){
        return $this->signals->count();
    }
    public function gettotalFollowingAttribute(){
        return $this->following->count();
    }
    public function gettotalFollowersAttribute(){
        return $this->followers->count();
    }

    public function getfollowAttribute(){
        $userId = \Auth::id();
        if($this->followers->where('follower_id', $userId)->count()==1){
            return true;
        }
            return false;  
    }

    public function fcmCheck($tokenId){
        return $this->fcmKey->where('token', $tokenId)->get();
        return 'success';
    }

}
